# stage_bastien_auvray

Ce dépôt GIT a pour but de réunir tous les documents
liés au stage de recherche de Bastien Auvray.

Le répertoire Bibliographie contient les différents articles
liés à la recherche de motif dans les séquences ordonnées.


Le répertoire src contient le code source des programmes
développés par Bastien dans le cadre de son stage.


Le répertoire Administratif contient un ensemble de documents
administratifs liés au stage.